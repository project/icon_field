<?php

namespace Drupal\icon_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'IconFieldIconShowFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "IconFieldIconShowFormatter",
 *   label = @Translation("Show icon only"),
 *   field_types = {
 *     "icon_field"
 *   }
 * )
 */
class IconFieldIconShowFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    foreach ($items as $delta => $item) {
      if ($item->icon_field == 'envelope') {
        $href_link = "mailto:" . $item->icon_field_link;
        $target_open = "";
      }
      else {
        $href_link = $item->icon_field_link;
        $target_open = "target='_blank'";
      }
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => "<a class='icon_field_link_alone' href='{$href_link}' {$target_open}><i class='fa fa-{$item->icon_field}'></i></a>",
      ];
      $elements[$delta]['#attached']['library'][] = 'icon_field/icon_field';
    }
    return $elements;
  }
}
