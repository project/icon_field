<?php

namespace Drupal\icon_field\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;

/**
 * Plugin implementation of the 'IconFieldDefaultFormatter' formatter.
 *
 * @FieldFormatter(
 *   id = "IconFieldDefaultFormatter",
 *   label = @Translation("Show icon and link"),
 *   field_types = {
 *     "icon_field"
 *   }
 * )
 */
class IconFieldDefaultFormatter extends FormatterBase {

  /**
   * Define how the field type is showed.
   *
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {

    $elements = [];
    foreach ($items as $delta => $item) {
      if ($item->icon_field == 'envelope') {
        $href_link = "mailto:" . $item->icon_field_link;
        $target_open = "";
      }
      else {
        $href_link = $item->icon_field_link;
        $target_open = "target='_blank'";
      }
      $elements[$delta] = [
        '#type' => 'markup',
        '#markup' => "<i class='fa fa-{$item->icon_field}'></i><a class='icon_field_with_title' href='{$href_link}' {$target_open}>" . $item->icon_field_link . "</a>",
      ];
      $elements[$delta]['#attached']['library'][] = 'icon_field/icon_field';
    }
    return $elements;
  }
}
