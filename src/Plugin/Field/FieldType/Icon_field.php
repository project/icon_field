<?php

namespace Drupal\icon_field\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface as StorageDefinition;


/**
 * Plugin implementation of the 'icon_field' field type.
 *
 * @FieldType(
 *   id = "icon_field",
 *   label = @Translation("Icon field"),
 *   description = @Translation("Stores an Icon field."),
 *   category = @Translation("Icon field"),
 *   default_widget = "IconFieldDefaultWidget",
 *   default_formatter = "IconFieldDefaultFormatter"
 * )
 */
class Icon_field extends FieldItemBase {

  /**
   * Field type properties definition.
   *
   */
  public static function propertyDefinitions(StorageDefinition $storage) {

    $properties = [];

    $properties['icon_field'] = DataDefinition::create('string')
      ->setLabel(t('Icon for field'));

    $properties['icon_field_link'] = DataDefinition::create('string')
      ->setLabel(t('Text in field'));

    return $properties;
  }

  /**
   * Field type schema definition.
   *
   */
  public static function schema(StorageDefinition $storage) {
    $columns = [];
    $columns['icon_field'] = [
      'type' => 'char',
      'length' => 255,
    ];
    $columns['icon_field_link'] = [
      'type' => 'char',
      'length' => 255,
    ];

    return [
      'columns' => $columns,
      'indexes' => [],
    ];
  }

  /**
   * Define when the field type is empty.
   *
   */
  public function isEmpty() {
    $isEmpty =
      empty($this->get('icon_field')->getValue()) &&
      empty($this->get('icon_field_link')->getValue());

    return $isEmpty;
  }
}
